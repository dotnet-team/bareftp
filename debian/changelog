bareftp (0.3.9-3) unstable; urgency=medium

  * [a23df4c] Use DH8-compatible --with instead of include
  * [46fc761] Add support for 4.5 libraries to M4, and use (Closes: #808633)
  * [ab0f496] Avoid pkglib_SCRIPTS error in modern Automake

 -- Jo Shields <directhex@apebox.org>  Mon, 21 Dec 2015 22:57:08 +0000

bareftp (0.3.9-2) unstable; urgency=medium

  * Rebuild with Mono 3.2 for CLR 4.5 transition

 -- Jo Shields <directhex@apebox.org>  Wed, 09 Sep 2015 14:19:02 +0100

bareftp (0.3.9-1) unstable; urgency=low

  [ Stefan Ebner ]
  * New upstream release 0.3.9
    + Fix problem giving exeption when opening preferences (#64, #65)
    + New translation: hu
    + Updated translations: fr

  [ Iain Lane ]
  * [d5c3966] Revert changed upstream files

 -- Stefan Ebner <sebner@ubuntu.com>  Sun, 30 Oct 2011 15:37:12 +0100

bareftp (0.3.8-2) unstable; urgency=low

  * [d3cae64] Add a build-depends on glib-dev, since we explicitly need glib-2.0.pc

 -- Jo Shields <directhex@apebox.org>  Tue, 21 Jun 2011 20:11:26 +0100

bareftp (0.3.8-1) unstable; urgency=low

  * New upstream release 0.3.8
    + Use combobox for remote charset selection
    + Don't disable synced browsing on refresh (#54)
    + Wider input entries in main window
    + Support international domain names (#56)
    + New translations: ja
    + Updated translations: pl, sv

 -- Stefan Ebner <sebner@ubuntu.com>  Thu, 14 Apr 2011 15:17:25 +0200

bareftp (0.3.7-2) unstable; urgency=low

  * Upload to unstable

 -- Stefan Ebner <sebner@ubuntu.com>  Fri, 11 Feb 2011 11:42:17 +0100

bareftp (0.3.7-1) experimental; urgency=low

  * New upstream release 0.3.7
    + Fixed crash when renaming to exisitng local file (#53)
    + Fixed crash issue on Ubuntu 10.04
    + Fixed crash issues when creating/renaming directory (#47)
    + Fixed behaviour of file operation dialog (#46)
    + Synchronized browsing ability (#45)
    + Avoid crash in file permission dialog when permission string are to short
    + UrlDecode drag/drop uri (#51)
    + Add current local and remote path when creating new bookmark (#48)
    + Handle disconnect on timed out connections (partly fixes #49)
    + Updated translations: pt_BR, fr, nb, es, ca
    + New translations: sk
  * Switch to dpkg-source 3.0 (quilt) format

 -- Stefan Ebner <sebner@ubuntu.com>  Tue, 23 Nov 2010 21:59:15 +0100

bareftp (0.3.6-1) experimental; urgency=low

  * New upstream bugfix release 0.3.6
    + Fixed path for ssh library
  * New upstream release 0.3.5
    + Directory caching is now optional
    + Fixed UI issues when background tasks are running - less sluggishness.
    + Fixed bug making bareftp miss ssh server password request
    + Fixed possible ldpath vulnerability in startup script
    + Handle network stream wite failures better
    + Fixed possible crash if renaming a file to an empty string
    + Better translation coverage
    + New/updated translations: nb, fr, ru, pl, es, sv, it
  * debian/copyright: Updated
  * debian/control: 
    + Bump Standards-Version to 3.9.1
    + Remove libtool from Build-Depends
  * debian/rules: Remove useless autoreconf and dh_override_clean

 -- Stefan Ebner <sebner@ubuntu.com>  Fri, 15 Oct 2010 17:53:44 +0200

bareftp (0.3.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix security issue CVE-2010-3350 (Closes: #598284)

 -- gustavo panizzo <gfa@zumbi.com.ar>  Tue, 05 Oct 2010 23:37:54 -0300

bareftp (0.3.4-1) unstable; urgency=low

  * New upstream bugfix release 0.3.4
    + Fixed #37: gnome-keyring problem
    + Fixed #38: Can't build without gnome-keyring
    + Revert correctly to previous dir on failed dir change
    + Fixed possible problem when swithing keyring mode with
      no password file present
  * New upstream bugfix release 0.3.3
    + Implemented workaround for patched gnome-keyring-sharp
      used in ubuntu 10.04. (#35)
    + Added custom command dialog (from right click menu)
    + Updated translations: pl

 -- Stefan Ebner <sebner@ubuntu.com>  Sat, 08 May 2010 12:42:35 +0200

bareftp (0.3.2-1) unstable; urgency=low

  * New upstream bugfix release 0.3.2
    + Fixed missing icon issues in progress panel (#34)
    + Improved drag'n drop. More user friendly on multiple selection
    + Some ui improvents for error reporting
    + Fixed crash when recieving a 550 on changedir
    + Fixed issue #30 (right click instability)
    + Fixed bug #29 (Impossible connect to a server with different port via sftp)
    + Ensure clean openssh exit on disconnect (no more defunct processes)
    + Changed date formating in list view
    + Updated translations: ca, de, es, fr, nb, pt_BR, sv
  * debian/patches/01_fix_gnome-keyring_sharp_detection: Merged upstream
  * debian/rules: 
    + Remove dh_auto_install build workaround
    + Remove quilt references
  * debian/control: 
    - Remove build-dependency on quilt
    - Bump Standards-Version to 3.8.4
  * debian/source/format: Specify 1.0 explicitly

 -- Stefan Ebner <sebner@ubuntu.com>  Mon, 19 Apr 2010 13:38:11 +0200

bareftp (0.3.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Add myself to Uploaders
    + Bump Standards-Version to 3.8.3
    + Change "Architecture: all" to "Architecture: any"
    + Add build-dependency on libgnome-keyring1.0-cil-dev and libtool
    + Update dependencies for -cil to -cil-dev transition
  * debian/rules:
    + Add workaround to let make find bareFTP.SftpPty.dll at build time
    + Remove useless .a and .la files
  * debian/copyright: Updated
  * debian/patches: Add 01_fix_configure_finding_gnome-keyring_sharp so
    configure finds gnome-keyring-sharp at build time

 -- Stefan Ebner <sebner@ubuntu.com>  Thu, 31 Dec 2009 12:35:38 +0100

bareftp (0.2.3-1) unstable; urgency=low

  * Initial release. (Closes: #520876)

 -- Mirco Bauer <meebey@debian.org>  Sun, 16 Aug 2009 00:11:06 +0200
