
CONTRIBUTORS
==================================================
Main author: Christian Eide <christian@eide-itc.no>

bareFTP icon by Kalle Persson <kalle@nemus.se>

The following has contributed with very useful bug reports, and 
made tests accounts on variuos server software available:
 - Peter Smrčák
 - Simon Leblanc
 - Peter Bergstrand (MockY)
 - Stefan Ebner
 - Michał Lisowski
 - And many others..

Huge thanks to you all!


TRANSLATORS
==================================================
Catalan (ca): 
	Sergi Mansilla <sergi.mansilla@gmail.com>
	Innocent De Marchi <tangram.peces@gmail.com>
German (de):
	Daniel Päßler <daniel@paessler.org>
Spanish (es): 
	Julio Napurí Carlos <julionc@gmail.com>
	Ricardo A. Hermosilla Carrillo <ra.hermosillac@gmail.com>
	Innocent De Marchi <tangram.peces@gmail.com>
French (fr): 
	Simon Leblanc <contact@leblanc-simon.eu>
	Vincent Blut <vincent.debian@free.fr>
Hungarian (hu):
	Adam Pongracz <pongadam@gmail.com>
Italian (it): 
	Gianvito Cavasoli <gianvito@gmx.it>
Japanese (ja):
	Cahell Hide <cahells@gmail.com>
Norwegian Bokmål (nb): 
	Christian Eide <christian@eide-itc.no>
Polish (pl): 
	Filip Chabik <hadret@gmail.com>
Portuguese Brazilian (pt_BR): 
	Alexandre Cavedon <alexandre.cavedon@gmail.com>
Russian (ru): 
	Алекс <davian818@gmail.com>
Slovak (sk):
	Peter Smrčák <smrcak@zymbo.sk>
Swedish (sv): 
	Niclas Kroon <niclas.kroon@gmail.com>
	Daniel Nylander <po@danielnylander.se>

ADDITIONAL LICENSES, COPYRIGHT HOLDERS AND CREDITS
==================================================

Some parts of bareFTP are based on code from other open sourced projects. 
As a result, some files have multiple copyright holders, and may use 
other open source licenses.

Brief description of derived works:

Some parts of the SSH implementation is based on code from JSch, a java
SSH library. JSch uses a BSD style license.
Copyright (c) 2002,2003,2004 ymnk, JCraft,Inc.
http://www.jcraft.com/jsch/index.html

bareFTP contains a modified implementation of pty-sharp.
pty-sharp is licensed under the MIT X11 license
pty-sharp is Copyright 2009, Novell, Inc.

The above mentioned modification of pty-sharp also uses some code and 
ideas from gftp. 
gftp is GPL, Copyright (C) 1998-2007 Brian Masney <masneyb@gftp.org>

Some code is influenced by edtFTPnet by EnterpriseDT, a open source FTP 
library. The code is Copyright (C) 2004 Enterprise Distributed
Technologies Ltd (www.enterprisedt.com)

A few Makefile and build concepts is taken from the banshee project.
Banshee is released under a MIT/X11 license, and
is Copyright (C) 2005-2007 Novell, Inc.

The file KeyBinder.cs is based on code from Tasque, relased
under a MIT license and is  Copyright (C) 2007 Novell, Inc.

** See source files for complete license/copyright **

** DETAILED COPYRIGHT AND LICENSES **

Files: *
Copyright: Copyright (c) 2007-2009 Christian Eide <christian@eide-itc.no>
License: GPL-2+

Files: src/bareFTP.Gui/KeyBinder.cs
Copyright: Copyright (C) 2007 Novell, Inc.
License: MIT/X11

FIles: src/bareFTP.Protocol.Ftp/ftp/ListParser.cs
Copyright: Copyright (c) 2004 Enterprise Distributed Technologies Ltd,
           Copyright (c) 2008-2009 Christian Eide <christian@eide-itc.no>
License: GPL-2+

Files: src/bareFTP.Protocol.Sftp/sftp/ssh/Buffer.cs
       src/bareFTP.Protocol.Sftp/sftp/ssh/SftpATTRS.cs
       src/bareFTP.Protocol.Sftp/sftp/ssh/Util.cs
Copyright: Copyright (c) 2002-2006 ymnk, JCraft,Inc.
License: BSD

Files: src/bareFTP.Protocol.Sftp/sftp/ssh/SftpClient.cs
Copyright: Copyright (c) 2002-2006 ymnk, JCraft,Inc.
           Copyright (c) 2008-2009 Christian Eide <christian@eide-itc.no>
License: GPL-2+

Files: lib/bareFTP.SftpPty/PseudoTerminal.cs
Copyright: Copyright (c) 2009 Novell, Inc.
           Copyright (c) 2009 Christian Eide <christian@eide-itc.no>
License: MIT/X11

Files: lib/sftppty/*
Copyright: Copyright (C) 2001,2002 Red Hat, Inc.
           Copyright (C) 1998-2007 Brian Masney <masneyb@gftp.org>
           Copyright (c) 2009 Christian Eide <christian@eide-itc.no>
License: LGPL-2+
