using System;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyVersion ("0.3.9.*")]
[assembly: AssemblyTitle ("bareFTP")]
[assembly: AssemblyDescription ("bareFTP File Transfer Client")]
[assembly: AssemblyCopyright ("Copyright (C) 2006-2010 Christian Eide")]
