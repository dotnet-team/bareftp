// AboutDialog.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using System.Text;
using System.Collections.Generic;

using Gtk;
using Mono.Unix;

namespace bareFTP
{
	
	public class AboutDialog : Gtk.AboutDialog
	{
		
		public AboutDialog() : base()
		{
			Gtk.AboutDialog.SetUrlHook(delegate(Gtk.AboutDialog dialog, string link) {
				Gnome.Url.Show(link);
			});
			
			Version = bareFTP.MainClass.Version;
			WidthRequest = 300;
			//Name = "bareFTP";
			ProgramName = "bareFTP"; // Requires Gtk# 2.12
			Copyright = "Copyright \xa9 2007-2011 Christian Eide\n\n";
			Authors = new string[] {"Christian Eide <christian@eide-itc.no>"};
			Logo = Gdk.Pixbuf.LoadFromResource("bareFTP.bareftp.png");
			Website = "http://www.bareftp.org";
			Comments = Mono.Unix.Catalog.GetString("File Transfer Client");
			Artists = new string[] {"bareFTP icon by Kalle Persson <kalle@nemus.se>"};
			TranslatorCredits = @"Catalan (ca): 
	Sergi Mansilla <sergi.mansilla@gmail.com>
	Innocent De Marchi <tangram.peces@gmail.com>
German (de):
	Daniel Päßler <daniel@paessler.org>
Spanish (es): 
	Julio Napurí Carlos <julionc@gmail.com>
	Ricardo A. Hermosilla Carrillo <ra.hermosillac@gmail.com>
	Innocent De Marchi <tangram.peces@gmail.com>
French (fr): 
	Simon Leblanc <contact@leblanc-simon.eu>
	Vincent Blut <vincent.debian@free.fr>
Hungarian (hu):
	Adam Pongracz <pongadam@gmail.com>
Italian (it): 
	Gianvito Cavasoli <gianvito@gmx.it>
Japanese (ja):
	Cahell Hide <cahells@gmail.com>
Norwegian Bokmål (nb): 
	Christian Eide <christian@eide-itc.no>
Polish (pl): 
	Filip Chabik <hadret@gmail.com>
Portuguese Brazilian (pt_BR): 
	Alexandre Cavedon <alexandre.cavedon@gmail.com>
Russian (ru): 
	Алекс <davian818@gmail.com>
Slovak (sk):
	Peter Smrčák <smrcak@zymbo.sk>
Swedish (sv): 
	Niclas Kroon <niclas.kroon@gmail.com>
	Daniel Nylander <po@danielnylander.se>
";
		}
		
		
			                       
	}
}
