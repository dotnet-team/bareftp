// LocalChmod.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;

namespace bareFTP.Common.Utils
{
	
	public class LocalChmod
	{

		public static void Chmod(string path, string permissions)
		{
			if(!Char.IsDigit(permissions[0]))
				permissions = PermissionParser.RWXToMode(permissions);
			
			Mono.Unix.UnixFileSystemInfo ufs = Mono.Unix.UnixFileSystemInfo.GetFileSystemEntry(path);
			int u = 0;
			int g = 0;
			int o = 0;
			
			u = Int32.Parse(permissions[0].ToString()) << 6;
			g = Int32.Parse(permissions[1].ToString()) << 3;
			o = Int32.Parse(permissions[2].ToString());
			int m = u | g | o;
			
			ufs.FileAccessPermissions = (Mono.Unix.FileAccessPermissions)m;
		}
		
	}
}
