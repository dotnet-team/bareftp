using System;

namespace bareFTP.Protocol
{
	public class SftpException : Exception
	{
		int code = 0;
		
		public SftpException(string msg) : base(msg)
		{
		}

		public SftpException(int code, string msg) : base(msg)
		{
			this.code = code;	
		}

		public int Code {
			get {
				return code;
			}
			set {
				code = value;
			}
		}
	}
}
