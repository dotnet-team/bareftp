// RemoteFile.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;

namespace bareFTP.Protocol {
	
	public class RemoteFile	{
		private string filename;
		private string relativepath;
		private long size;
		private DateTime lastModified;
		private string owner;
		private string group;
		private string permissions;
		private bool isDir;
		private bool isLink;
		private string linkdest;
		private string abspath;
		private int linkcount;
		private BarePath path;
		
		public RemoteFile() {
			path = new BarePath();
		}
		
		public string Filename {
			get { return filename; }
			set { filename = value; }
		}
		
		public long Size {
			get { return size; }
			set { size = value; }
		}
		
		public DateTime LastModified {
			get { return lastModified; }
			set { lastModified = value; }
		}
		
		public string Owner	{
			get { return owner; }
			set { owner = value; }
		}
		
		public string Group	{
			get { return group; }
			set { group = value; }
		}

		public string Permissions {
			get	{ return permissions; }
			set { permissions = value; }
		}

		public bool IsDir {
			get { return isDir;	}
			set { isDir = value; } 
		}
		
		public bool IsLink {
			get { return isLink;	}
			set { isLink = value; } 
		}

		public string Linkdest {
			get { return linkdest; }
			set { linkdest = value; }
		}
		public string AbsPath {
			get { return abspath; }
			set { abspath = value; }
		}
		public int LinkCount {
			get { return linkcount; }
			set { linkcount = value; }
		}

		public BarePath Path {
			get { return path; }
			set { path = value; }
		}

		public string RelativePath {
			get {
				return relativepath;
			}
			set {
				relativepath = value;
			}
		}
	}
}
