// FTPReply.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.Collections.Generic;
using System.Text;

namespace bareFTP.Protocol
{
	
	public class FTPReply
	{
		int replyCode = 0;
		string replyMsg;
		bool silent = false;
		MessageType type;
		
		public FTPReply()
		{}
		
		public FTPReply(int replyCode, string replyMsg)
		{
			this.replyCode = replyCode;
			this.replyMsg = replyMsg;
		}
		
		public FTPReply(MessageType replyType, string replyMsg)
		{
			this.replyMsg = replyMsg;
			this.type = replyType;
		}
		
		public int ReplyCode {
			get { return replyCode; }
		}
		
		public MessageType ReplyType {
			get { return type; }
		}
		
		public bool Silent {
			get { return silent; }
			set { silent = value; }
		}
		
		public string Message {
			get 
			{ 
				if(replyMsg.IndexOf("PASS") == 0)
					return "PASS ********";
				return replyMsg; 
			}
		}
		
		public MessageType MessageType
		{
			get {
				if(replyCode == 0)
					return type;
				else
				{
					// No type means we'll check a ftp reply code
					if(replyCode == -1)
						return MessageType.ClientCommand;
					else if(replyCode == -2)
						return MessageType.ClientError;
					else if(replyCode == 220)
						return MessageType.Welcome;
					else if(replyCode < 300)
					{
						return MessageType.Info;
					}
					else if(replyCode >= 300 && replyCode < 400)
					{
						return MessageType.Info;
					}
					else if(replyCode > 400)
					{
						return MessageType.Error;
					}
					else
						return MessageType.Undef;
				}
			}
		}
		
		public override string ToString ()
		{
			if(replyCode > 0)
				return ReplyCode.ToString() + " " + Message;
			else
				return Message;
		}

	}
	
}
