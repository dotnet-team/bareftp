// ExceptionDialog.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;

namespace bareFTP.Gui.Dialog
{
	
	
	public partial class ExceptionDialog : Gtk.Dialog
	{
		
		
		protected virtual void onCloseClicked (object sender, System.EventArgs e)
		{
			this.Destroy();
		}
		

		
		public ExceptionDialog(Exception ex)
		{
			this.Build();
			this.label1.Text = ex.Message;
				
			this.textview1.Buffer.InsertAtCursor(ex.ToString());
		}
	}
}
