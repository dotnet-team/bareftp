using System;

namespace bareFTP.Gui.Dialog
{


	public partial class SendCommand : Gtk.Dialog
	{

		public SendCommand ()
		{
			this.Build ();
		}
		
		public string Command {
			get {
				return entry_command.Text;
			}
		}
		
		protected virtual void OnCommandEntryActivated (object sender, System.EventArgs e)
		{
			buttonOk.Click();
		}
		
		
	}
}
