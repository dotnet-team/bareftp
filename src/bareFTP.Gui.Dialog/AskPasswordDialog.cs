
using System;

namespace bareFTP.Gui.Dialog
{
	
	
	public partial class AskPasswordDialog : Gtk.Dialog
	{
		public string Password {
			get {
				return entry1.Text;
			}
		}

		
		public AskPasswordDialog()
		{
			this.Build();
			entry1.Activated += delegate {
				buttonOk.Click();
			};
		}

	}
}
