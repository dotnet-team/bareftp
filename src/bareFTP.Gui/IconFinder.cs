// IconFinder.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using Gtk;
using Gnome.Vfs;

namespace bareFTP.Gui
{
	
	
	public class IconFinder
	{
		public static string FindIconName(string filename)
		{	
			string mime = Gnome.Vfs.Global.GetMimeTypeForName(filename);
			if(!string.IsNullOrEmpty(mime))
			{
				
				Gnome.IconLookupResultFlags res;
			
			string icon = Gnome.Icon.Lookup(Gtk.IconTheme.Default, 
			                                    null, null, null, null, mime, 
			                                    Gnome.IconLookupFlags.None, out res);
				return icon;
			}
			else
				return "unknown";
			
		}
					
	}
}
