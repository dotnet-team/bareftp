// BookmarkUtils.cs
//
//  Copyright (C) 2010 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using Gtk;
using bareFTP.Preferences.Bookmarks;

namespace bareFTP.Gui
{


	public class BookmarkUtils
	{
		public event System.EventHandler BookmarkActivated;
		
		// We apparently need two menus.. using the same menu on two places fails..
		Menu bookmarkitems;
		Menu bookmarkitems2;
		BookmarkMenuButton bookmarkButton;
		MenuBar menubar;
		
		public BookmarkUtils ()
		{
			Gtk.Widget bicon = Gtk.Image.NewFromIconName(Gtk.Stock.Directory, IconSize.Menu);
			
			bookmarkButton = new BookmarkMenuButton(bicon, Mono.Unix.Catalog.GetString("Bookmarks"));
			bookmarkButton.ShowAll();
			
			bookmarkitems = new Menu();
			bookmarkitems2 = new Menu();
		}
		
		public void Refresh()
		{
			BuildMenus();
		}
		
		private void BuildMenus()
		{
			if(bookmarkitems != null)
			{
				bookmarkitems.Destroy();
				bookmarkitems = new Menu();
			}
			if(bookmarkitems2 != null)
			{
				bookmarkitems2.Destroy();
				bookmarkitems2 = new Menu();
			}
			
			Preferences.Bookmarks.Bookmarks b = new Preferences.Bookmarks.Bookmarks();
			
			if(!b.BackendOperational)
			{
				Console.WriteLine("BackendOperational: Gnome Keyring not available");
				MessageDialog md = new MessageDialog(null, DialogFlags.DestroyWithParent, 
				                                      MessageType.Error, ButtonsType.Ok,
				                                      Mono.Unix.Catalog.GetString("Communication with GnomeKeyring failed and has been disabled.") + 
				                                      Environment.NewLine + Environment.NewLine + 
				                                      Mono.Unix.Catalog.GetString("If the problem persists, you may want to disable it permanently"));
				md.Run ();
				md.Destroy();
			}
			
			if(b.RootItem != null)
				CreateBookmarkItems(b.RootItem, bookmarkitems, bookmarkitems2);
			
			SetMenubarBookmarkItems();
			
			bookmarkButton.Menu = bookmarkitems;
			
			bookmarkitems.ShowAll();
			bookmarkitems2.ShowAll();
		}
		
		private void CreateBookmarkItems(Preferences.Bookmarks.BookmarkItem bitem, Gtk.Menu menu, Gtk.Menu menu2)
		{
			foreach(Preferences.Bookmarks.BookmarkItem item in bitem.Items)
			{
				if(item is Preferences.Bookmarks.BookmarkFolder)
				{
					Gtk.ImageMenuItem menuitem = new ImageMenuItem(item.Name);
					menuitem.Image = Gtk.Image.NewFromIconName(Gtk.Stock.Directory, IconSize.Menu);
					
					Gtk.ImageMenuItem menuitem2 = new ImageMenuItem(item.Name);
					menuitem2.Image = Gtk.Image.NewFromIconName(Gtk.Stock.Directory, IconSize.Menu);
					
					menu.Append(menuitem);
					menu2.Append(menuitem2);
					
					Menu m = new Menu();
					Menu m2 = new Menu();
					
					menuitem.Submenu = m;
					menuitem2.Submenu = m2;
					
					CreateBookmarkItems(item, m, m2);
				}
				else if(item is Preferences.Bookmarks.BookmarkEntry)
				{
					BookmarkMenuItem menuitem = new BookmarkMenuItem(item.Name);
					menuitem.Image = Gtk.Image.NewFromIconName(Gtk.Stock.File, IconSize.Menu);
					menuitem.BookmarkEntry = (Preferences.Bookmarks.BookmarkEntry)item;
					menuitem.Activated += onBookmarkActivated;
					menu.Append(menuitem);
					
					BookmarkMenuItem menuitem2 = new BookmarkMenuItem(item.Name);
					menuitem2.Image = Gtk.Image.NewFromIconName(Gtk.Stock.File, IconSize.Menu);
					menuitem2.BookmarkEntry = (Preferences.Bookmarks.BookmarkEntry)item;
					menuitem2.Activated += onBookmarkActivated;
					menu2.Append(menuitem2);
					
				}
			}
		}
		
		public void SetMenubarBookmarkItems()
		{
			if(menubar == null)
				return;
			
			foreach(Gtk.Widget w in menubar.Children)
			{
				if(w.Name == "BookmarksAction1")
				{
					ImageMenuItem m1 = (ImageMenuItem)w;
					Menu m11 = (Menu)m1.Submenu;
					
					foreach(Gtk.Widget w2 in m11.Children)
					{
						if(w2.Name == "BookmarksAction")
						{
							ImageMenuItem re = (ImageMenuItem)w2;
							re.Submenu = null;
							re.Submenu = bookmarkitems2;
							re.Submenu.ShowAll();
						}
					}
				}
				
			}
		}
		
		protected void onBookmarkActivated(object sender, EventArgs e)
		{
			if(BookmarkActivated != null)
				BookmarkActivated.Invoke(sender, e);	
		}
		
		public Menu BookmarkMenu
		{
			get { return bookmarkitems; }	
		}
		
		public MenuBar BookmarkMenuBar
		{
			get { return menubar; }
			set { menubar = value; }
		}
		
		public BookmarkMenuButton BookmarkButton
		{
			get { return bookmarkButton; }	
		}
	}
}
