// Common.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using bareFTP.Protocol;

namespace bareFTP.Gui
{
	
	
	public class Common
	{
		
		public static Gtk.ComboBox MakeProtocolComboBox()
		{
			Gtk.ListStore ls = new Gtk.ListStore(typeof(int), typeof(string));
			ls.AppendValues((int)ProtocolType.FTP,  ProtocolType.FTP.ToString());
			ls.AppendValues((int)ProtocolType.FTPS,  ProtocolType.FTPS.ToString());
			ls.AppendValues((int)ProtocolType.SFTP,  "SSH (SFTP)");
			Gtk.ComboBox combobox1 = new Gtk.ComboBox(ls);
			Gtk.CellRendererText text = new Gtk.CellRendererText();
			combobox1.PackStart(text, false);
			combobox1.AddAttribute(text, "text", 1);
			combobox1.WidthRequest = 120;
			
			return combobox1;
		}
		
	}
}
