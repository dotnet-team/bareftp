// bookmark_gen.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using bareFTP.Preferences.Bookmarks;

namespace bareFTP.Gui.Preferences
{
	
	[System.ComponentModel.ToolboxItem(true)]
	public partial class bookmark_gen : Gtk.Bin
	{
		public event EventHandler NameChanged;
		
		Gtk.ComboBox cbProtocol;
		BookmarkEntry bookmark;
		bool disable_event = false;
		
		public bookmark_gen()
		{
			string common_info1 = Mono.Unix.Catalog.GetString("<i><b>Note:</b> Password are currently stored in clear text</i>");
			string common_info2 = Mono.Unix.Catalog.GetString("If you don't save the password you will be prompted for it when connecting.");
			string infostr = string.Empty;
			
			bareFTP.Preferences.PwdBackend p = new bareFTP.Preferences.PwdBackend();
			
			if(!p.HasGnomeKeyring)
				infostr = Mono.Unix.Catalog.GetString("This bareFTP is built without GnomeKeyring support.");
			else
			{
				bareFTP.Preferences.Config conf = new bareFTP.Preferences.Config(string.Empty);
				if(	!conf.General_UseGnomeKeyring)
					infostr = Mono.Unix.Catalog.GetString("You can enable Gnome Keyring password storage in Preferences to encrypt your passwords.");
			}
			
			if(!string.IsNullOrEmpty(infostr))
				infostr = common_info1 + "\n" + infostr + "\n" + common_info2;	
			
			this.Build();
			
			infolabel.LabelProp = infostr;
			cbProtocol = bareFTP.Gui.Common.MakeProtocolComboBox();
			if(bookmark != null)
				cbProtocol.Active = bookmark.Protocol - 1;
			else
				cbProtocol.Active = 0;
			table2.Attach(cbProtocol, 1,2,0,1);
			table2.ShowAll();
			entryName.Changed += nameChanged;
			entryHost.Changed += valueChanged;
			entryUser.Changed += valueChanged;
			entryPass.Changed += valueChanged;
			entryPort.Changed += valueChanged;
			cbProtocol.Changed += valueChanged;
			cb_passive.Clicked += valueChanged;
			cb_encryptdatachannel.Clicked += valueChanged;
			cb_showhiddenfiles.Clicked += valueChanged;
			cb_synced_browsing.Clicked += valueChanged;
			entryLocalPath.Changed += valueChanged;
			entryRemotePath.Changed += valueChanged;
			entryRemoteCharset.Changed += valueChanged;
		}

		protected void nameChanged(object sender, EventArgs e)
		{
			bookmark.Name = entryName.Text;
			if(NameChanged != null)
				NameChanged(this, null);
		}

		protected void valueChanged(object sender, EventArgs e)
		{
			if(!disable_event)
			{
				bookmark.User = entryUser.Text.Trim();
				bookmark.Host = entryHost.Text.Trim();
				bookmark.Pass = entryPass.Text.Trim();
				bookmark.Port = entryPort.Text.Trim();
				bookmark.Protocol = cbProtocol.Active + 1;
				bookmark.Passive = cb_passive.Active;
				if(cb_encryptdatachannel.Active)
					bookmark.EncryptData = "P";
				else
					bookmark.EncryptData = "C";
				bookmark.ShowHidden = cb_showhiddenfiles.Active;
				bookmark.LocalPath = entryLocalPath.Text.Trim();
				bookmark.RemotePath = entryRemotePath.Text.Trim();
				bookmark.CharSet = entryRemoteCharset.Text.Trim();
				bookmark.SyncedBrowse = this.cb_synced_browsing.Active;
			}
		}
		
		public BookmarkEntry Bookmark
		{
			set
			{
				disable_event = true;
				bookmark = value;
				entryHost.Text = bookmark.Host;
				entryName.Text = bookmark.Name;
				entryPass.Text = bookmark.Pass;
				entryUser.Text = bookmark.User;
				entryPort.Text = bookmark.Port.ToString();
				cb_encryptdatachannel.Active = (bookmark.EncryptData == "P");
				cb_passive.Active = bookmark.Passive;
				cb_showhiddenfiles.Active = bookmark.ShowHidden;
				entryRemoteCharset.Text = bookmark.CharSet;
				cbProtocol.Active = bookmark.Protocol - 1;
				entryLocalPath.Text = bookmark.LocalPath;
				entryRemotePath.Text = bookmark.RemotePath;
				cb_synced_browsing.Active = bookmark.SyncedBrowse;
				disable_event = false;
			}
		}
	}
}
