
using System;
using Gtk;
using bareFTP.Preferences.Bookmarks;

namespace bareFTP.Gui.Preferences
{
	
	
	public class BookmarkStore : TreeStore
	{
		
		public BookmarkStore(params Type[] types) : base(types)
		{
			
		}
		
		public void MoveBottom(TreeIter src)
		{
			BookmarkItem item = (BookmarkItem)GetValue(src,0);
			AppendValues(item);
			Remove(ref src);
		}
		
		public void MoveInto(TreeIter src, TreeIter dst)
		{
			BookmarkItem item = (BookmarkItem)GetValue(src,0);
			InsertWithValues(dst, IterNChildren(dst), item);
			Remove(ref src);
		}
	}
}
