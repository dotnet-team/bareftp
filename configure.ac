AC_INIT([bareftp], [0.3.9])

AC_CANONICAL_SYSTEM
AC_PREREQ(2.54)
AM_INIT_AUTOMAKE([1.9 dist-bzip2 tar-ustar])
AM_MAINTAINER_MODE

dnl DISTCHECK_CONFIGURE_FLAGS="--disable-docs"
dnl AC_SUBST(DISTCHECK_CONFIGURE_FLAGS)

ASM_VERSION="$VERSION.*"
AC_SUBST(ASM_VERSION)

dnl Pieces needed by autogen; can't be parsed from macros
AM_CONFIG_HEADER(config.h)
AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AM_PROG_CC_STDC
IT_PROG_INTLTOOL([0.35])
AC_PROG_LIBTOOL
AC_PROG_INSTALL
PKG_CHECK_MODULES(GLIB,glib-2.0 > 2.9.0)

AC_CHECK_FUNCS(getpt grantpt unlockpt ptsname ptsname_r)
AC_CHECK_FUNC(socket,[have_socket=1],AC_CHECK_LIB(socket,socket,[have_socket=1; LIBS="$LIBS -lsocket"]))
AC_CHECK_FUNC(socketpair,[have_socketpair=1],AC_CHECK_LIB(socket,socketpair,[have_socketpair=1; LIBS="$LIBS -lsocket"]))
AC_CHECK_FUNC(recvmsg,[have_recvmsg=1],AC_CHECK_LIB(socket,recvmsg,[have_recvmsg=1; LIBS="$LIBS -lsocket -lnsl"]))

if test x$have_socket = x1 ; then
	AC_DEFINE(HAVE_SOCKET,1,[Define if you have the socket function.])
fi
if test x$have_socketpair = x1 ; then
	AC_DEFINE(HAVE_SOCKETPAIR,1,[Define if you have the socketpair function.])
fi
if test x$have_recvmsg = x1 ; then
	AC_DEFINE(HAVE_RECVMSG,1,[Define if you have the recvmsg function.])
fi
			
AC_CHECK_HEADERS(sys/select.h sys/syslimits.h sys/termios.h sys/un.h stropts.h termios.h)
			

dnl Setup GETTEXT
SHAMROCK_CONFIGURE_I18N($PACKAGE)

dnl pkg-config
AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
if test "x$PKG_CONFIG" = "xno"; then
	AC_MSG_ERROR([You need to install pkg-config])
fi
	

dnl Expansions
SHAMROCK_EXPAND_LIBDIR
SHAMROCK_EXPAND_BINDIR
SHAMROCK_EXPAND_DATADIR

dnl Check Mono
dnl SHAMROCK_CHECK_MONO_MODULE(1.1.10)

SHAMROCK_FIND_MONO_2_0_COMPILER   
SHAMROCK_FIND_MONO_RUNTIME

dnl Check for assemblies
SHAMROCK_CHECK_MONO_2_0_GAC_ASSEMBLIES([        
 	System.Web
 	Mono.Posix
])

BAREFTP_CHECK_GTK_SHARP

dnl {{{ Optial support for Gnome Keyring
CSC_DEFINES=""

AC_ARG_WITH([gnomekeyring],
  [AS_HELP_STRING([--without-gnomekeyring],
  [Disable support for Gnome Keyring password storage])],
  [],
  [with_gnomekeyring=yes])

AS_IF([test x"$with_gnomekeyring" != xno],
  [PKG_CHECK_MODULES([GNOME_KEYRING_SHARP], [gnome-keyring-sharp-1.0])
  AC_SUBST([GNOME_KEYRING_SHARP_LIBS])
  CSC_DEFINES="$CSC_DEFINES -d:HAVE_GNOME_KEYRING"],
  [echo "Gnome Keyring will not be used..."])

AC_ARG_ENABLE(caches,
 	[ --enable-caches Run update-* to update mime, desktop and icon caches when installing [[default = yes]]],,
 	[enable_caches="yes"])
AM_CONDITIONAL(UPDATE_CACHES, test x"$enable_caches" = "xyes") 

LOCALE_DIR=${prefix}
AC_SUBST(LOCALE_DIR)
AC_SUBST(CSC_DEFINES)

AC_OUTPUT([
Makefile
bareftp
m4/Makefile
data/Makefile
data/icon-theme/Makefile
lib/Makefile
lib/sftppty/Makefile
lib/bareFTP.SftpPty/Makefile
po/Makefile.in
src/Makefile
src/bareFTP.Preferences/Makefile
src/bareFTP.Common.Utils/Makefile
src/bareFTP.Connection/Makefile
src/bareFTP.Protocol/Makefile
src/bareFTP.Protocol.Ftp/Makefile
src/bareFTP.Protocol.Sftp/Makefile
src/bareFTP.Gui/Makefile
src/bareFTP.Gui.FileManager/Makefile
src/bareFTP.Gui.ProgressMonitor/Makefile
src/bareFTP.Gui.Dialog/Makefile
src/bareFTP.Gui.Preferences/Makefile
src/bareFTP/Makefile
src/bareFTP/AssemblyInfo.cs
src/bareFTP/Defines.cs
])

echo "
bareFTP-$VERSION

	Install Prefix:    ${prefix}
	Datadir:           ${expanded_datadir}
	Libdir:            ${expanded_libdir}
	    
	Mono C# Compiler:  ${MCS}
	Mono Runtime:      ${MONO}
	---------------------------------------
	
	Binaries will be installed in ${expanded_libdir}/bareftp
	Startup script will be installed in ${expanded_bindir}
	
"


if test -e ${expanded_libdir}/bareftp/bareftp.exe; then
	echo "WARNING: An existing bareFTP install is in ${expanded_libdir}/bareftp"
        echo "         It is recomended to remove the existing install before "
        echo "         installing this build to avoid conflicts."
        echo " "
fi
